# TableGo

#### 介绍

　　TableGo是基于数据库的代码自动生成工具，低代码编程技术的实现，可以零代码自动生成SpringBoot项目工程、生成JavaBean、生成MyBaits的Mapper映射配置文件、生成数据库设计文档（Word、Excel）、生成Swagger2离线API文档、生成前后端代码、能查出数据库数据生成各种代码和文档等，更重要的是可以根据每个项目的不同开发框架编写自定义模板与项目框架适配生成各模块增删查改的前后端代码，让开发人员的开发效率提高60%以上，并且可以通过模板定义开发规范统一开发风格和标准，提高代码的规范性和可维护性。<br/>
　　只要设计好数据库并且添加好备注，就能通过自定义模板生成任意编程语言的任何程序代码，并且能够生成各种代码备注。实现只要把数据数据库设计好，整个项目就完成了很大一部分代码的编写，大大节省了项目的开发成本。支持MySQL、Oracle、SQL Server、PostgreSQL、MariaDB五种数据库，支持Window、Linux、Mac OS等多种操作系统。<br/>
　　使用自定义模板功能可以根据数据库表结构信息生成你想要的任何代码，例如：Java、C#、C++、Golang、Rust、Python、Objective-C、Swift、VB、VC、SQL、HTML、JSP、JS、PHP、Vue、React、Word、Excel等等，没有做不到只有想不到……<br/>
　　生成数据模型功能升级，现在可以直接生成八种不同编程语言的数据模型：<br/>
　　生成JavaBean数据模型、生成C#数据模型、生成C++数据模型(C++结构体)、生成Golang数据模型、生成Rust数据模型、生成Python数据模型、生成Objective-C数据模型、生成Swift数据模型、生成iOS数据模型<br/><br/>
　　在CSDN博客可下载Jar包版本的TableGo，可在Linux和Mac OS上运行：[https://blog.csdn.net/vipbooks](https://blog.csdn.net/vipbooks)

#### 运行环境

　　要想运行TableGo必须要安装JDK8及以上版本的Java环境，不再支持JDK7，并且要配置好JAVA_HOME或者JRE_HOME，如果是运行Jar包版本的还要把 %JAVA_HOME%\bin 也配置到path变量中去。<br/>
　　在JDK9及以上版本环境中运行TableGo.jar必须要使用脚本运行，在程序压缩包中已提供了TableGo.bat和TableGo.sh的运行脚本，可根据系统环境自行选择运行的脚本。<br/>
　　因为从JDK9开始不能再使用URLClassLoader动态加载Jar包了，所以数据库驱动无法远能动态加载，经过长时间研究发现可以使用Instrumentation.appendToSystemClassLoaderSearch方法来动态加载Jar包，并且测试没有问题，所以就写了一个tablego-agent.jar来动态加载数据库驱动包。<br/>
　　tablego-agent.jar需要在TableGo.jar运行前使用Java Agent技术加载，相当于JVM级别的AOP，在TableGo.jar运行前先执行，动态加载完Jar包以后再运行TableGo.jar，所以现在运行TableGo.jar的命令就是这样的：java -jar -javaagent:./lib/tablego-agent.jar TableGo.jar<br/>
　　JDK8的版本运行TableGo.jar无需使用Java Agent，可直接双击运行，在代码中做了处理，如果是JDK8的版本还是会使用URLClassLoader动态加载一次数据库驱动包。<br/>
　　EXE版本的TableGo在进行EXE打包的时候就已经配置好了Java Agent，可直接运行。

#### MySQL关键字表名问题

MySQL关键字做表名导致生成代码或文档报错的解决方法，以order表名为例：
1.  先在公共参数中配置精确匹配(包含)参数，把表名`order`填入进去，记得一定要加键盘左上角的“`”引号。
2.  切换到生成工具中生成代码或文档，先把所有关键字表名的表单独生成完。
3.  生成完成以后再到公共参数中把精确匹配(包含)参数中的内容清空。
4.  在公共参数中配置精确匹配(排除)参数，把表名order填入进去，记得一定不能加“`”引号。
5.  切换到生成工具中生成代码或文档，生成所有非关键字命名的表，到这里数据库中所有的表就都生成完了。

#### 自定义模板的写法提示

　　写自定义最简单的方法就是在你的项目代码里找出一套前后台增删查改的标准流程代码出来，把这套代码拷到IDE另一个专门用来写模板的项目中去，直接把文件的后缀都改成ftl，然后在这套标准流程代码上把动态的内容都改成变量，改完之后一套完全适配你项目的模板就做完了。<br/>
　　我已经把SpringMVC、SpringBoot和一些页面的模板示例都写好了，你们可以参考我的模板示例再根据自己的项目框架编写完全适配你们项目的自定义模板就可以了。<br/>
　　在贡献者群会提供我在一些真实项目中写的全套自定义模板代码，如果某个项目的自定义模板与你的项目框架一致，则只需要简单改改就能使用了。<br/>
　　程序中的模板都是用FreeMarker写的，如果不会可以去官网点击下载，进到百度网盘里面有视频教程，很容易学会，学会FreeMarker再参考使用手册和模板示例就能写完全适配自己项目框架的代码模板了。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
