<#-- 自定义模板参数toString测试 -->
dbConfig: ${JsonUtils.objToJson(dbConfig)}

paramConfig: ${JsonUtils.objToJson(paramConfig)}

jsonParam: ${JsonUtils.objToJson(jsonParam)}

<#if CollectionUtils.isNotEmpty(tableInfoList)>
tableInfoList: ${JsonUtils.objToJson(tableInfoList)}

    <#list tableInfoList as tableInfo>
    tableInfo: ${JsonUtils.objToJson(tableInfo)}
    <#if tableInfo.sqlQueryData??>

        sqlQueryData: ${JsonUtils.objToJson(tableInfo.sqlQueryData)}
    </#if>

        <#if tableInfo.fieldInfos?? && tableInfo.fieldInfos?size gt 0>
            <#list tableInfo.fieldInfos as fieldInfo>
        fieldInfo: ${JsonUtils.objToJson(fieldInfo)}

            </#list>
        </#if>
    </#list>
</#if>