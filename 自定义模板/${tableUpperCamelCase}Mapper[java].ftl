<#-- 用于生成Mapper接口的自定义模板 -->
package ${jsonParam.packagePath}

import org.apache.ibatis.annotations.Param;
import java.util.List;
import com.baomidou.mybatisplus.core.metadata.IPage;
import ${jsonParam.groupId}.${jsonParam.artifactIdLowerCase}.model.<#if jsonParam.moduleName??>${jsonParam.moduleName}.</#if>${tableInfo.upperCamelCase};
import ${jsonParam.groupId}.${jsonParam.artifactIdLowerCase}.model.condition.<#if jsonParam.moduleName??>${jsonParam.moduleName}.</#if>${tableInfo.upperCamelCase}Condition;

/**
 * ${tableInfo.simpleRemark}Mapper接口
 * 
 * @author ${paramConfig.author}
 * @version 1.0.0 ${today}
 */
public interface ${tableInfo.upperCamelCase}Mapper {
    /**
     * 根据查询参数分页查询${tableInfo.simpleRemark}列表
     *
     * @param condition 查询参数
     * @return 分页信息
     */
    IPage<${tableInfo.upperCamelCase}> findByCondition(${tableInfo.upperCamelCase}Condition condition);

    /**
     * 根据主键ID批量删除${tableInfo.simpleRemark}
     *
     * @param idList
     *            主键ID列表
     */
    void deleteByIds(List<String> idList);
}